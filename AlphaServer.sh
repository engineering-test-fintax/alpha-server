#!/bin/bash
DIR=/opt/scripts/alpha-server
CLIENT=`cat $DIR/clients/twc-1.log | tail -n 1| awk '{printf $11}'`

total-attempt () {
for data in `find $DIR/clients/ -type f -iname "*.log"`
do
	CLIENT=`cat $data | tail -n 1| awk '{printf $11}'`
	echo -n "* $CLIENT had " > $data.txt
        wc -l $data | awk '{printf $1}' >> $data.txt
	echo " attempt(s)" >> $data.txt
done
}

show-attempt () {
for data in `find $DIR/ -type f -iname "*.txt"`
do
	echo -n " " && cat $data
done
}

total-attempt

echo "==================================="
echo "========== ALPHA SERVER ==========="
echo "==================================="
echo "  Metrics for ssh log-in attempts"
echo ""
show-attempt
echo "==================================="
