# Alpha Server



## Getting started

This project is intended to fulfill the written test for the position of DevOps Engineer at Pajak.io.
I'm hoping to pass this test so that I can join Pajak.io.

## Execute this command on server node

```
mkdir /opt/scripts
cd /opt/scripts
git clone this repo
ln -s /opt/scripts/alpha-server/AlphaServer.sh /usr/local/bin/alphaserver
```

## To use this alpha-server, use command 
```
alphaserver
```

Salam,
Muhammad Ridho

